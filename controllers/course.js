const Course = require("../models/Course");
const auth = require("../auth");

// Controller function for creating a course
module.exports.addCourse = (courseData) => {
    // Create a variable "newCourse" and instantiates a new course object
    if(courseData.isAdmin){
        let newCourse = new Course({
            name : courseData.course.name,
            description : courseData.course.description,
            price : courseData.course.price
        })

        return newCourse.save().then((course, error) =>{
            if(error){
                return false;
            }
            else{
                return true;
            }
        })
    }
    else{
        return Promise.resolve(false);
    }
}

// Controller function for getting all courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result
    })
}

// Controller function for getting a specific course
module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result
    })
}

// Controller function for updating a course
module.exports.updateCourse = (reqParams, reqBody) => {
    // Specify the fields/properties of the document to be updated
    let updatedCourse = {
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price
    }

    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        if(error){
            return false
        }
        else{
            return true
        }
    })
}

// Controller function for archiving a course
// module.exports.archiveCourse = (reqParams, archive) => {
//     if(archive.isAdmin) {
//         let archivedCourse = {
//             isActive : archive.course.isActive
//         }

//         return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
//             if(error){
//                 return false
//             }
//             else{
//                 return true
//             }
//         })
//     }
//     else{
//         return Promise.resolve(false);
//     }
// }

module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		// Course not archived
		if (error) {

			return false;

		// Course archived successfully
		} else {

			return true;

		}

	});
};


// Admin
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYTFlMjIzYzdiNGRkM2Y3YjdlNWQ0ZiIsImVtYWlsIjoiam9obkBtYWlsLmNvbSIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1NDg2NjcyM30.D21p1kYNx8oSZXdX0NI_esr4Tz83T0EHNNpB3Dn8fy0

// Not admin
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYTg1Y2MxMGU0MGE5NjU3ZWMzYzI1MiIsImVtYWlsIjoiamFkZUBtYWlsLmNvbSIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1NTIwMDk3OH0.t2r67qF7Yo6HgWh0hqMXc2oZ8SktnsPXrJfiCLlpD9w